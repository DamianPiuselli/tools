# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 15:14:45 2019

@author: Otros
"""

import numpy as np
import scipy as sp
from sklearn import datasets
from scipy import stats
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd

def EJCR(x,y):
    ''' Given x and y data, calculates the EJCR for any function two parameter f
    function. 
    Returns a_grid,b_grid,sse,obj_lim   to plot a contour plot at height obj_lim'''   
    def f(x, a, b):
        return a * x + b

    # perform regression
    popt, pcov = curve_fit(f, x, y)
    
    # retrieve parameter values
    a = popt[0]
    b = popt[1]
    
    # calculate parameter stdev
    s_a = np.sqrt(pcov[0,0])
    s_b = np.sqrt(pcov[1,1])
    
    # calculate 95% confidence interval
    aL = a-1.96*s_a
    aU = a+1.96*s_a
    bL = b-1.96*s_b
    bU = b+1.96*s_b
    
    # calculate max square interval 
    a5L = a-5.0*s_a
    a5U = a+5.0*s_a
    b5L = b-5.0*s_b
    b5U = b+5.0*s_b
    

    # Generate contour plot of SSE ratio vs. Parameters
    # meshgrid is +/- change in the objective value
    m = 250
    
    i1 = np.linspace(a5L,a5U,m)
    i2 = np.linspace(b5L,b5U,m)
    a_grid, b_grid = np.meshgrid(i1, i2)
    n = len(y) # number of data points
    p = 2      # number of parameters
    
    # sum of squared errors
    sse = np.empty((m,m))
    for i in range(m):
        for j in range(m):
            at = a_grid[i,j]
            bt = b_grid[i,j]
            sse[i,j] = np.sum((y-f(x,at,bt))**2)
    
    # optimal solution
    best_sse = np.sum((y-f(x,a,b))**2)
    
    # compute f-statistic for the f-test
    alpha = 0.05 # alpha, confidence
                 # alpha=0.05 is 95% confidence

#    fstat = sp.stats.f.isf(alpha,p,(n-p))
    #modified according to  Comprehensive chemometrics, chi2 stat for comparing 2 analytical methods.
    fstat = sp.stats.chi2.isf(alpha,p)
    flim = fstat * p / (n-p)
    obj_lim = flim * best_sse + best_sse
    
    return(a_grid,b_grid,sse,obj_lim)

if __name__ == '__main__':
    
    #DATA1
    x, y, coef = datasets.make_regression(n_samples=30, n_features=1,
                                      n_informative=1,bias=0.5, noise=10,
                                      coef=True, random_state=0)
    x = x.flatten()
    #Calculate grid
    a_grid,b_grid,sse,obj_lim = EJCR(x,y)
    
    # Create a contour plot
    plt.figure()
    plt.xlabel('Pendiente (a)')
    plt.ylabel('Ordenada al origen (b)')
    plt.title("some title")
    #plot DATA1
    CS = plt.contour(a_grid,b_grid,sse,[obj_lim],\
                     colors='blue',linewidths=[1.5])
    plt.clabel(CS, inline=1, fontsize=10, fmt='DATA1')

#    #center (if 0,1 is ideal)
#    plt.scatter(1,0,s=10,c='red',marker='x')
